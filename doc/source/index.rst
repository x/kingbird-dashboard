===============================
Kingbird Dashboard
===============================

User Interface for Kingbird

* Free software: Apache license
* Source: http://git.openstack.org/cgit/openstack/kingbird-dashboard
* Bugs: http://bugs.launchpad.net/kingbird-dashboard

Features
--------

* TODO

User Documentation
------------------

.. toctree::
   :maxdepth: 2

   install/index
   configuration/index
   Release Notes <https://docs.openstack.org/releasenotes/kingbird-dashboard>

Contributor Guide
-----------------

.. toctree::
   :glob:
   :maxdepth: 2

   contributor/index
